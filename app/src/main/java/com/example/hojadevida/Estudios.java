package com.example.hojadevida;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class Estudios extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_estudios);
    }

    //boton regresar
    public void Regresar (View view){
       Intent regresar = new Intent(this, MainActivity.class);
        startActivity(regresar);
   }

}